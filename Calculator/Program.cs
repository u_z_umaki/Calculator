﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        private static void InputValue(Circuit circuit, string symbol)
        {
            circuit.Input(symbol);
            circuit.Run();
            Console.ReadKey(true);
            Update(circuit);
        }

        public static void Update(Circuit circuit)
        {
            int selectedIndex = circuit.Run();

            switch (selectedIndex)
            {
                case 0:
                    InputValue(circuit, "%");
                    break;
                case 1:
                    circuit.Сleaning();
                    circuit.Run();
                    Console.ReadKey(true);
                    Update(circuit);
                    break;
                case 2:
                    circuit.Dell();
                    circuit.Run();
                    Console.ReadKey(true);
                    Update(circuit);
                    break;
                case 3:
                    InputValue(circuit, "/");
                    break;
                case 4:
                    InputValue(circuit, "7");
                    break;
                case 5:
                    InputValue(circuit, "8");
                    break;
                case 6:
                    InputValue(circuit, "9");
                    break;
                case 7:
                    InputValue(circuit, "*");
                    break;
                case 8:
                    InputValue(circuit, "4");
                    break;
                case 9:
                    InputValue(circuit, "5");
                    break;
                case 10:
                    InputValue(circuit, "6");
                    break;
                case 11:
                    InputValue(circuit, "-");
                    break;
                case 12:
                    InputValue(circuit, "1");
                    break;
                case 13:
                    InputValue(circuit, "2");
                    break;
                case 14:
                    InputValue(circuit, "3");
                    break;
                case 15:
                    InputValue(circuit, "+");
                    break;
                case 16:
                    InputValue(circuit, "_");
                    break;
                case 17:
                    InputValue(circuit, "0");
                    break;
                case 18:
                    InputValue(circuit, ".");
                    break;
                case 19:
                    circuit.Split();
                    circuit.DellAll();
                    circuit.Run();
                    Console.ReadKey(true);
                    Update(circuit);
                    break;
            }
        }
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.Unicode;
            char[] options = { '%', 'C', '←', '/', '7', '8', '9', '*', '4', '5', '6', '-', '1', '2', '3', '+', '_', '0', ',', '=' };

            Circuit circuit = new Circuit(options);
            
            Update(circuit);
        }
    }
}
