﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Circuit
    {
        private string String = " ";
        private double Result = 0;
        private int SelectedIndex;
        private char[] Options;
        char[] MassiveOperations = { '+', '-', '/', '*' };

        public Circuit(char[] options)
        {
            Options = options;
            SelectedIndex = 0;
        }
        public void Operation(char sign, int Num)
        {
            switch (sign)
            {
                case '+':
                    Result += Num;
                    break;
                case '-':
                    Result -= Num;
                    break;
                case '*':
                    Result *= Num;
                    break;
                case '/':
                    Result /= Num;
                    break;
            }
        }
        public void Split()
        {
            for (int i = 0; i < MassiveOperations.Length; i++)
            {
                if (String.IndexOf(MassiveOperations[i]) > 0)
                {
                    string[] str = String.Split(MassiveOperations[i]);
                    int num = Convert.ToInt32(str[1]);
                    Operation(MassiveOperations[i], num);
                }
            }
        }
        public void Input(string symbol)
        {
            if (Result == 0) Result = Convert.ToDouble(symbol);
            else if (
                String == " " &&
                symbol != "+" &&
                symbol != "-" &&
                symbol != "*" &&
                symbol != "/"
                ) Result = Result * 10 + Convert.ToDouble(symbol);
            else String += symbol;
        }

        public void Dell()
        {
            if (String.Length > 0)
            {
                String = String.Remove(String.Length - 1);               
            }
        }
        public void DellAll()
        {
            if (String.Length > 0)
            {
                String = " ";
            }
        }
        public void Сleaning()
        {
            if (String.Length > 0)
            {
                Result = 0;
                String = String.Remove(String.Length - String.Length);
            }
        }
        public int Run()
        {
            ConsoleKey keyPressed;
            do
            {
                Console.Clear();

                PrintСircuit();

                ConsoleKeyInfo keyInfo = Console.ReadKey(true); 
                keyPressed = keyInfo.Key;

                switch (keyPressed)
                {
                    case ConsoleKey.LeftArrow:
                        SelectedIndex--;
                        if (SelectedIndex == -1)
                        {
                            SelectedIndex = Options.Length - 1;
                        }
                        break;
                    case ConsoleKey.RightArrow:
                        SelectedIndex++;
                        if (SelectedIndex == Options.Length)
                        {
                            SelectedIndex = 0;
                        }
                        break;
                    case ConsoleKey.UpArrow:
                        SelectedIndex -= 4;
                        switch (SelectedIndex)
                        {
                            case -4:
                                SelectedIndex = 16;
                                break;
                            case -3:
                                SelectedIndex = 17;
                                break;
                            case -2:
                                SelectedIndex = 18;
                                break;
                            case -1:
                                SelectedIndex = 19;
                                break;
                        }
                        break;
                    case ConsoleKey.DownArrow:
                        SelectedIndex += 4;
                        switch (SelectedIndex)
                        {
                            case 20:
                                SelectedIndex = 0;
                                break;
                            case 21:
                                SelectedIndex = 1;
                                break;
                            case 22:
                                SelectedIndex = 2;
                                break;
                            case 23:
                                SelectedIndex = 3;
                                break;
                        }
                        break;
                }
            } while (keyPressed != ConsoleKey.Enter);
            return SelectedIndex;
        }

        private void Blank(int size)
        {
            for (int i = 0; i < size; i++)
            {
                Console.Write(" ");
            }
        }
        private void Length(int size)
        {
            for (int i = 0; i < size; i++)
            {
                Console.Write("\u2501");
            }
        }
        private char UpDown = '\u2503';

        private char UpLeft = '\u250F';
        private char UpRight = '\u2513';
        private char DownLeft = '\u2517';
        private char DownRight = '\u251B';

        private char UpLeftDown = '\u252B';
        private char UpRightDown = '\u2523';
        private char UpRightLeft = '\u253B';
        private char RightDownLeft = '\u2533';

        private char UpRightDownLeft = '\u254B';

        public void PrintСircuit()
        {            
            Console.Write(UpLeft);
            Length(15);
            Console.WriteLine(UpRight);    
            
            Console.Write(UpDown);
            Blank(2);
            Console.Write("Калькулятор");
            Blank(2);
            Console.WriteLine(UpDown);
            
            Console.Write(UpRightDown);
            Length(15);
            Console.WriteLine(UpLeftDown);

            Console.Write(UpDown);
            Blank(1);
            Console.Write(Result);
            string temp = Convert.ToString(Result);
            Console.Write(String);
            Blank(14 - String.Length - temp.Length);           
            Console.WriteLine(UpDown);

            Console.Write(UpRightDown);
            for (int i = 0; i < 3; i++)
            {
                Length(3);
                Console.Write(RightDownLeft);              
            }
            Length(3);
            Console.WriteLine(UpLeftDown);
            
            for (int i = 0; i < 20; i++)
            {
                Console.Write(UpDown);
                Console.Write(" ");
                char currentOption = Options[i];
                if (i == SelectedIndex)
                {
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.Write($"{currentOption}");
                    
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.Write($"{currentOption}");
                }
                Console.ResetColor();

                if (i == 3 || i == 7 || i == 11 || i == 15)
                {
                    Console.Write(" ");
                    Console.WriteLine(UpDown);
                    Console.Write(UpRightDown);
                    for (int j = 0; j < 3; j++)
                    {
                        Length(3);
                        Console.Write(UpRightDownLeft);
                    }
                    Length(3);
                    Console.WriteLine(UpLeftDown);
                }
                else
                {
                    Console.Write(" ");
                }

                if (i == 19)
                {
                    Console.WriteLine(UpDown);
                    Console.Write(DownLeft);
                    for (int h = 0; h < 3; h++)
                    {
                        Length(3);
                        Console.Write(UpRightLeft);
                    }
                    Length(3);
                    Console.WriteLine(DownRight);
                }
            }           
        }
    }
}
